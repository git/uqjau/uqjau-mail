#!/usr/bin/perl
use strict;
use warnings;
use Carp qw(croak);
use MIME::Entity;

# ==================================================================== 
# USAGE: ourname [-d] -f FROM -t TO [-s SUBJECT] [-b TYPE] < BODY
# 
# OPTION:
#   -d          Debug.  Do not send. Print mail to STDOUT.
# EXAMPLE:
#   ourname -f foo@gmail.com -t bar@zap.edu -s 1234 -b text/html < /tmp/mybody.html
# 
# NOTES: Has hardcoded path to, and forks sendmail. Code largely
# from perldoc MIME::Entity.  No support for attachments now. Should
# fail w/return value from MTA. perldoc on MIME::Lite suggested
# MIME::Entity instead.
# ==================================================================== 

use Getopt::Std;
our $ourname;
($ourname  = $0) =~ s:.*/::g;

our( $opt_d,
     $opt_f,
     $opt_t,
     $opt_s,
     $opt_b,
   );

getopts('df:t:s:b:');

croak( "$ourname:[-f FROM] is required.\n") unless $opt_f;
croak( "$ourname:[-t TO] is required.\n")   unless $opt_t;

$opt_b ||= "text/plain";
$opt_s ||= "";

# slurp body from STDIN
my $body;
$body = do { local $/; <> };

my $msg;
$msg = MIME::Entity->build(Type    => $opt_b,
                           From    => $opt_f,
                           To      => $opt_t,
                           Data    => $body, 
                           Subject => $opt_s);

if ( ! $opt_d ) {
    # sendmail man page
    # 
    #        -t     Read  message  for recipients.  To:, Cc:, and Bcc: lines will be
    #               scanned for recipient addresses.  The Bcc: line will be  deleted
    #               before transmission.

    ### Send it:
    open (MAIL, "|/usr/lib/sendmail -t  -oi -oem") 
        or croak ("Cant start sendmail: $!");

    $msg->print(\*MAIL);
    close MAIL
        or croak ("sendmail: $!");
}
else {
    # Debug mode.
    $msg->print(\*STDOUT);
}
