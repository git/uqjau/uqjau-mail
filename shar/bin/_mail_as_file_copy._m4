#!/bin/bash

set -ue
ourname=${0##*/}

help() {
sed -n " 2,\$ {s/\$ourname/${0##*/}/g; s/^    //; p;}" <<\____eohd

    SYNOPSIS: multiline synopsis 
    would go here

    _COMMAND_SHORTNAME([<{ }>])

    USAGE: $ourname ...

    OPTIONS:
        -c                  Copy FILE_PATHNAME to temp file preserving entire pathname info in basename.'

        -i COMMENT          COMMENT shows up in tmpfile name passed to vim'

    EXAMPLES:

    FILES:
      external tools invoked, config files

    ENVIRONMENT:
      external env var dependencies

    DESCRIPTION:

    LOGS:

    EXIT CODES:

    GLOBALS:

    INPUTS: 

    OUTPUTS: 

    CALLED BY:

    UPDATED: [<{_m4_date}>]
____eohd
}

# ====================================================================
# Copyright (c) 2016 Tom Rodman <Rodman.T.S@gmail.com>
#
# --------------------------------------------------------------------

# == Software License ==
# This file is part of uqjau.
#
# uqjau is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# uqjau is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with uqjau.  If not, see <http://www.gnu.org/licenses/>.
# --------------------------------------------------------------------

_hostname_short () 
{ 
    : _func_ok2unset_ Source is in a _29r ".shinc" lib file, so usable for interactive shell or script.;
    : --------------------------------------------------------------------;
    : Synopsis: echo hostname, without DNS domain;
    : --------------------------------------------------------------------;
    if out=${HOSTNAME:-${COMPUTERNAME:-$(
        exec 2>/dev/null;hostname || uname -n || exit 1)}}; then
        echo ${out%%.*};
    else
        echo $FUNCNAME:ERROR: could not determine hostname 1>&2;
        return 1;
    fi
}

# --------------------------------------------------------------------
#
#   read rcfile ( for env var defs );
#
# --------------------------------------------------------------------
for dir in ~
do
  rcfile=$dir/.${ourname}rc
    # env vars impacted by rcfile:
    #   Set 'from_default' here.

  [[ -s $rcfile ]] && source "$rcfile"

done
unset rcfile dir

# ==================================================
# Process optional parameters.
# ==================================================

OPTIND=1
  # OPTIND=1 only needed for 2nd and subsequent getopt invocations
  # since shell's start up sets it to 1

#     leading : => 'getopts silent error reporting'
while getopts :f:F: opt_char
do
   # Uncomment echo below to debug:
   # echo "opt_char:[$opt_char] OPTARG:[${OPTARG:-}] OPTIND:[$OPTIND]"
   case $opt_char in
     \?) echo $ourname: unsupported option \(-$OPTARG\) >&2
         [[ -n ${FUNCNAME:-} ]] && return 1 || exit 3
     ;;
     :)  echo $ourname: missing argument for option \(-$OPTARG\) >&2
         # Only works if the switch is last arg  on command line.
         [[ -n ${FUNCNAME:-} ]] && return 1 || exit 3
     ;;
   esac

   case ${OPTARG:-} in
     -*)
       {
       echo $ourname: [$OPTARG] not supported as arg to [-$opt_char]
       echo \ \ since it begins with -.
       } >&2
       [[ -n ${FUNCNAME:-} ]] && return 1 || exit 3
     ;;
   esac

   # save info in an "OPT_*" env var.
   eval OPT_${opt_char}="\"\${OPTARG:-TRUE}\""
done

# $OPTIND is index of next arg to be processed (1 == 1st arg)
shift $(( $OPTIND -1 ))
unset opt_char

## main
to=${1:-$LOGNAME@localhost}

file_to_send=${OPT_F:-}

if [[ -s $file_to_send ]];then
    
    pn=$(canPath "$file_to_send")
    ls_info=$(ls -log "$pn")
    md5sum=$(set -- $(md5sum < "$pn"); echo $1)
    file_info=$(
        echo Attached file from $(_hostname_short): [${ls_info}, w/md5sum: $md5sum]
    )

else
    echo $ourname: required: -F PATHNAME >&2 
    exit 1
fi

(
set -x
: "STDIN is from [$file_to_send]"

< "$file_to_send" \
_simple_MTA_v1.pl \
    -b application/octet-stream \
    -f ${from_default:-${OPT_f:-foo@gmail.com}} \
    -s "$file_info" \
    -t $to
)
