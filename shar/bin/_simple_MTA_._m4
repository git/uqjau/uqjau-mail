#!/usr/bin/env perl
use strict;
use warnings;
use Carp qw(croak);
use MIME::Entity;

# ==================================================================== 
# SYNOPSIS: ourname [-d] -f FROM -t TO [-s SUBJECT] [-b TYPE] < BODY
# 
# OPTION:
#   -d          Debug.  Do not send. Print mail to STDOUT.
# EXAMPLE:
#   ourname -f foo@gmail.com -t bar@zap.edu -s 1234 -b text/html < /tmp/mybody.html
# 
# NOTES: Has hardcoded path to, and forks sendmail. Code largely
# from perldoc MIME::Entity.  No support for attachments now. Should
# fail w/return value from MTA. perldoc on MIME::Lite suggested
# MIME::Entity instead.
# ==================================================================== 

# ====================================================================
# Copyright (c) 2016 _Tom_Rodman_email
# --------------------------------------------------------------------

# -- Software License --
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ---

use Getopt::Std;
our $ourname;
($ourname  = $0) =~ s:.*/::g;

our( $opt_d,
     $opt_f,
     $opt_t,
     $opt_s,
     $opt_b,
   );

getopts('df:t:s:b:');

croak( "$ourname:[-f FROM] is required.\n") unless $opt_f;
croak( "$ourname:[-t TO] is required.\n")   unless $opt_t;

$opt_b ||= "text/plain";
$opt_s ||= "";

# slurp body from STDIN
my $body;
$body = do { local $/; <> };

my $msg;
$msg = MIME::Entity->build(Type    => $opt_b,
                           From    => $opt_f,
                           To      => $opt_t,
                           Data    => $body, 
                           Subject => $opt_s);

if ( ! $opt_d ) {

    # Normal mode. (not Debug mode)
    # sendmail man page
    # 
    #        -t     Read  message  for recipients.  To:, Cc:, and Bcc: lines will be
    #               scanned for recipient addresses.  The Bcc: line will be  deleted
    #               before transmission.
    #
    #        -oi    Do *not* consider a line with only a period to indicate EOT/eof.
    #               34.8.32 IgnoreDots (i): http://www.diablotin.com/librairie/networking/sendmail/ch34_08.htm

    ### Send it:
    open (MAIL, "|/usr/lib/sendmail -t  -oi -oem") 
        or croak ("Cant start sendmail: $!");

    $msg->print(\*MAIL);
    close MAIL
        or croak ("sendmail: $!");
}
else {
    # Debug mode.
    $msg->print(\*STDOUT);
}
